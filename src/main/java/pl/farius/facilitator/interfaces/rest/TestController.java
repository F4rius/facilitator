package pl.farius.facilitator.interfaces.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @RequestMapping(path = "/test")
    String getTest() {
        return "Dupa działa";
    }
}
